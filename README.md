# Exemple d'utilisation de POO avec python

## Exécuter le script 

`python3.7 poo.py` 

## Les mécanismes POO utilisés sont :

- Présence de plusieurs Classes : utilisées en mode statique ou instanciés
- Héritage simples et multiples
- surcharge de méthode
- utilisation de super()
- exemples d'imports
- comparaison d'objets 
- print d'objet